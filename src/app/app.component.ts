import { Component, AfterViewInit, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Console } from 'console';
import { Web3Service } from './services/web3.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('scrollframe', { static: false }) private scrollFrame!: ElementRef;
  @ViewChildren('item') itemElements!: QueryList<any>;

  private scrollContainer: any;

  title = 'Votaciones';
  msgEstado = 'No Conectado.';
  estado = false;
  countMarvel = 0;
  countDC = 0;
  resultado = '';
  votovalido = 0;
  obtencionVoto = 0;

  constructor(public web3s: Web3Service) { }

  ngAfterViewInit(): void {
    this.conectar();
    this.scrollContainer = this.scrollFrame.nativeElement;
  }

  private onElementosChanged(): void {
    this.scrollToBottom();
  }

  conectar(): void {
    this.web3s.connectAccount().then((r) => {
      this.msgEstado = "Conectado.";
      this.estado = true;
      this.obtenerVotos(0);
      this.obtenerVotos(1);
    });
  }

  actualizarVotos():void{
    this.obtenerVotos(0);
    this.obtenerVotos(1);   
  }

  obtenerVotos(propuesta: any): void {
    this.web3s.contrato.methods.obtenerBotos(propuesta).call().then((response: any) => {
      if (propuesta == 0) {
        this.countMarvel = response;
      } else if (propuesta == 1) {
        this.countDC = response;
      }
    });
  }

  votar(propuesta: number): void {
    this.web3s.contrato.methods.votar(propuesta).send({ from: this.web3s.accounts[0] }).then((response: any) => {
      this.votovalido = 1;
    }).catch((error: any) => {
      console.log(error);
      this.votovalido = 2;
   });
  }

  derechoVoto():void{
    this.web3s.contrato.methods.otorgarVoto().send({from: this.web3s.accounts[0]}).then((response:any)=>{
      this.obtencionVoto=1;
    }).catch((error:any)=>{
      this.obtencionVoto =2;
    });
  }

  scrollToBottom() {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }
}
