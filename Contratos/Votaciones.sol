// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "remix_tests.sol"; // this import is automatically injected by Remix.
import "../contracts/Token.sol";

contract Votaciones{
    
    LCSTToken token;
    
    function beforeAll () public {
        token = new LCSTToken();
    }
    
    struct Votante{
        address direccion;
        bool voto;
    }
    
    struct Propuesta{
        bytes32 nombre;
        uint no_votos;
    }
    
     mapping(address => Votante) public Votantes;
     
     Propuesta[] public Propuestas;
     
     constructor(){
         Propuestas.push(
             Propuesta({
                 nombre:"Marvel",
                 no_votos:0
             })
             );
        Propuestas.push(
             Propuesta({
                 nombre:"DC",
                 no_votos:0
             })
             );     
     }
     
     function otorgarVoto() public returns (bool success) {
         require(!Votantes[msg.sender].voto,"Ya realizaste tu voto!");
         require(token.balanceOf(msg.sender) == 0,"Ya reclamaste tu derecho de voto");
         token.transfer(msg.sender,10000000000);
         Votantes[msg.sender].direccion=address(msg.sender);
         Votantes[msg.sender].voto = false;
         return true;
     }
     
     function votar(uint propuesta) public returns (bool success){
         Votante storage sender = Votantes[msg.sender];
         require(!sender.voto,"Ya realizaste tu voto!");
         require(token.balanceOf(msg.sender) >= 10000000000,"No has reclamado tu derecho de voto");
         sender.voto = true;
         token.transferFrom(msg.sender,address(0xBC2976e3644aa8a562C440a3a8b1926DCDa721b1),10000000000);
         Propuestas[propuesta].no_votos+=1;
         return true;
     }
     
     function obtenerBotos(uint propuesta) public view returns (uint votos){
         votos = Propuestas[propuesta].no_votos;
     }
     
     function direcContrato() public view returns (address){
         return address(token);
     }
}

//Direccion 0xE08329D0EAe863490e028F4c079A9b04F3e4Dc92

//ABI 

[
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "Propuestas",
		"outputs": [
			{
				"internalType": "bytes32",
				"name": "nombre",
				"type": "bytes32"
			},
			{
				"internalType": "uint256",
				"name": "no_votos",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"name": "Votantes",
		"outputs": [
			{
				"internalType": "address",
				"name": "direccion",
				"type": "address"
			},
			{
				"internalType": "bool",
				"name": "voto",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "beforeAll",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "direcContrato",
		"outputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "propuesta",
				"type": "uint256"
			}
		],
		"name": "obtenerBotos",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "votos",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "otorgarVoto",
		"outputs": [
			{
				"internalType": "bool",
				"name": "success",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "propuesta",
				"type": "uint256"
			}
		],
		"name": "votar",
		"outputs": [
			{
				"internalType": "bool",
				"name": "success",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	}
]
